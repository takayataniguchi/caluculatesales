package jp.alhinc.taniguchi_takaya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		//コマンドライン引数が1つではない場合のエラー処理
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//支店コード情報、支店別売上情報に関するMapの宣言
		Map<String, String> branchName = new HashMap<>();
		Map<String, Long> branchSale = new HashMap<>();

		//商品コード情報、商品別売上情報に関するMapの宣言
		Map<String, String> commodityName = new HashMap<>();
		Map<String, Long> commoditySale = new HashMap<>();

		//支店定義ファイルの読み込み
		if(!readFile(args, "支店定義ファイル", branchName, branchSale, "branch.lst", "[0-9]{3}")) {
			return;
		}

		//商品定義ファイルの読み込み
		if(!readFile(args, "商品定義ファイル", commodityName, commoditySale, "commodity.lst", "[A-Za-z0-9]{8}")) {
			return;
		}

		//売上ファイルを引数(のフォルダ)から取得
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {

			//売上ファイル名の適正判定後、該当のみListへ
			if((files[i].isFile()) && (files[i].getName().matches("[0-9]{8}\\.rcd$"))) {
				rcdFiles.add(files[i]);
			}
		}

		//List(rcdFiles)に格納された売上ファイル名を昇順でソート
		Collections.sort(rcdFiles);

		//Listに格納された売上ファイル名が連番になっているか確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			File formerFile = rcdFiles.get(i);
			String rcdFileNameFormer = formerFile.getName();

			File latterFile = rcdFiles.get(i + 1);
			String rcdFileNameLatter = latterFile.getName();

			int former = Integer.parseInt(rcdFileNameFormer.substring(0,8));
			int latter = Integer.parseInt(rcdFileNameLatter.substring(0,8));


			//売上ファイルの連番確認
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

		}

		//Listに格納した売上ファイル内情報の読み込み
		for(int i= 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {

				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				//読み込んだファイル内に文字があればリスト(items)へ格納
				String line;
				List<String> items= new ArrayList<>() ;

				while((line = br.readLine()) != null) {
					items.add(line);
				}

				//Listの売上ファイル名を文字列として抜粋
				File temporaryFile = rcdFiles.get(i);
				String rcdFileName = temporaryFile.getName();

				//Listの売上ファイル内情報のフォーマットを確認
				if(items.size() != 3) {
					System.out.println(rcdFileName + "のフォーマットが不正です");
					return;
				}

				//売上ファイルに記録された支店別キーがbranchName中に存在しているか確認
				if(!branchName.containsKey(items.get(0))){
					System.out.println(rcdFileName + "の支店コードが不正です");
					return;
				}

				//売上ファイルに記録された商品別キーがcommodityName中に存在しているか確認
				if(!commodityName.containsKey(items.get(1))){
					System.out.println(rcdFileName + "の支店コードが不正です");
					return;
				}

				//Listの売上ファイル内の売上金額が数字のみで記録されているかを確認
				if(!items.get(2).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//売上金額を支店コード毎でbranchSaleに加算
				Long branchSaleAmount = branchSale.get(items.get(0)) + Long.parseLong(items.get(2));

				//売上金額が支店コード毎で10桁以内であるか確認
				if(branchSaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//売上金額を商品コード毎でcommoditySaleに加算
				Long commoditySaleAmount = commoditySale.get(items.get(1)) + Long.parseLong(items.get(2));

				//売上金額が商品コード毎で10桁以内であるか確認
				if(commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//branchSaleに保持
				branchSale.put(items.get(0), branchSaleAmount);

				//commoditySaleに保持
				commoditySale.put(items.get(1), branchSaleAmount);

			}catch(IOException e ) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally {
				if (br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

		}

		//支店別集計ファイルの書き出し処理
		if(!writeFile(args, "branch.out", branchName, branchSale)) {
			return;
		}

		//商品別集計ファイルの書き出し処理
		if(!writeFile(args, "commodity.out",commodityName, commoditySale)) {
			return;
		}

	}

	//定義ファイル読み込み処理のメソッド（支店定義ファイル、商品定義ファイルに適用）
	//引数(mainメソッドの引数, 読み込むファイルの種類:「〇〇ファイル」と記載, ファイルから読取ったコード及び名に係る情報を格納するMap,
	//ファイルから読取ったコード及び金額に係る情報を格納するMap, 「"読み取るファイル名.拡張子"」と記載, 定義ファイル内情報のコードの形式：正規表現で記載)
	static boolean readFile(String[] args, String categoryName, Map<String, String> name, Map<String, Long> sale, String fileName, String regex){

		//定義ファイルの宣言
		File nameFile = new File(args[0], fileName);

		//定義ファイルの存在確認
		if(!nameFile.exists()) {
			System.out.println(categoryName +"が存在しません");
			return false;
		}

		BufferedReader br = null;
		try {
			FileReader fr = new FileReader(nameFile);
			br = new BufferedReader(fr);

			//読込んだファイル内に文字があれば配列へ格納
			String line;
			String items[] = null ;
			while((line = br.readLine()) != null) {
				items = line.split(",");

				//定義ファイル内情報のフォーマット適正確認
				if((items.length != 2) || (!items[0].matches(regex))){
					System.out.println(categoryName +"のフォーマットが不正です");
					return false;
				}

				//読み取った定義ファイル内情報をMapに格納
				name.put(items[0], items[1]);
				sale.put(items[0], 0L);
			}

		}catch(IOException e ) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {
			if (br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		return true;
	}

	//書き出し処理のメソッド
	//引数(mainメソッドの引数, 出力されるファイル名:「"ファイル名.拡張子"」と記載, コード及び名に係る情報を格納したMap, コード, 合計金額に係る情報を格納したMap)
	static boolean writeFile(String[] args, String fileName, Map<String, String> name,Map<String, Long> sale){

		BufferedWriter bw = null;

		try{
			File file = new File(args[0],  fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//nameMapのコード、名、並びにsaleMapの合計金額を合算して書出し処理
			for(String key :name.keySet()) {
				bw.write(key + "," + name.get(key) + "," + sale.get(key));

				//改行
				bw.newLine();
			}
			return true;

		}catch(IOException e ) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {
			if (bw != null) {
				try {
					bw.close();
				}catch(IOException e ) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
}
